package examen;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import java.io.*;

public class S2 extends JFrame
{
	JTextField text;
	JButton b;
	
	S2()
	{
		setTitle("Aplicatie text");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400,300);
        setVisible(true);
	}
	
	public void init()
    {

        this.setLayout(null);

        text = new JTextField();
        text.setBounds(10,50,80,20);

        b = new JButton("OK");
        b.setBounds(10,80,80,20);
        b.addActionListener(new TratareButon());
        
        add(text);
        add(b);
    }
	
	class TratareButon implements ActionListener 
	{
		public void actionPerformed(ActionEvent e)
    	{
    		String txt = S2.this.text.getText();
    		for ( int i=txt.length()-1;i>=0;i--)
    		{
    			char carExtras = txt.charAt(i);
    			System.out.print(carExtras);
    		}
    	}
	}
	public static void main(String[] args)
	{
		S2 s2 = new S2();
	}
}
