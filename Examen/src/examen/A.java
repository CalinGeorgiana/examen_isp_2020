package examen;

public class A 
{

}

class B extends A 
{
	private long t;
	public B()   
	{
		
	}
	public void metoda(C c)   // pentru relatia de dependenta
	{
		
	}
}

class C
{
	
}

class D
{
	E e;
	D()
	{
		E e = new E();   // pentru agregare puternica 
	}
	public void met1(int i)  //am considerat pentru metoda met1() tipul void
	{
		
	}
}

class E 
{
	public void met2()  //am considerat pentru metoda met2() tipul void
	{
		
	}
}

class F
{
	D d;   // pentru agregare slaba
}
